import os
import sys
from search import *
import requests
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QPushButton, QLineEdit, QComboBox, QCheckBox
from PyQt5.QtCore import Qt


class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def getImage(self, coords):
        data = {
            'll': coords,
            'spn': str(self.size) + ',' + str(self.size),
            'l': ["map", "sat", "sat,skl"][self.map_type.currentIndex()],
            'pt': self.points
        }
        map_request = "http://static-maps.yandex.ru/1.x/?" + '&'.join(
            [i[0] + '=' + (i[1] if type(i[1]) != list else ','.join(i[1])) for i in data.items() if len(i[1])])
        response = requests.get(map_request)
        if not response:
            print("Ошибка выполнения запроса:")
            print(map_request)
            print("Http статус:", response.status_code, "(", response.reason, ")")
            self.addres_output.setText('Не найдено')
            return
        self.map_file = "map.jpg" if data['l'] != 'map' else "map.png"
        with open(self.map_file, "wb") as file:
            file.write(response.content)

    def initUI(self):
        self.resize(700, 500)
        self.points = []
        self.size = 0.05
        self.setWindowTitle('Отображение карты')
        self.image = QLabel(self)
        self.image.move(0, 0)
        self.image.resize(700, 450)
        self.image.mousePressEvent = self.map_click
        self.x_field = QLineEdit(self)
        self.y_field = QLineEdit(self)
        self.start = QPushButton(self)
        self.x_field.move(25, 475)
        self.y_field.move(225, 475)
        self.x_field.setFocusPolicy(Qt.ClickFocus)
        self.y_field.setFocusPolicy(Qt.ClickFocus)
        self.start.setFocusPolicy(Qt.ClickFocus)
        self.map_type = QComboBox(self)
        self.map_type.addItems(["схема", "спутник", "гибрид"])
        self.map_type.move(400, 475)
        self.map_type.setFocusPolicy(Qt.ClickFocus)
        self.map_type.currentIndexChanged.connect(self.update_image)
        self.search_input = QLineEdit(self)
        self.search_input.move(25, 450)
        self.search_input.setFocusPolicy(Qt.ClickFocus)
        self.addres_output = QLineEdit(self)
        self.addres_output.move(275, 450)
        self.addres_output.setFocusPolicy(Qt.ClickFocus)
        self.start.move(500, 475)
        self.start.setText('start')
        self.start.clicked.connect(self.search_point)
        self.search = QPushButton(self)
        self.search.move(500, 450)
        self.search.setText('Найти')
        self.search.clicked.connect(self.search_func)
        self.search.setFocusPolicy(Qt.ClickFocus)
        self.addres_output.setReadOnly(True)
        self.addres_output_label = QLabel(self)
        self.addres_output_label.setText('Адрес')
        self.addres_output_label.move(240, 450)
        self.clear_search = QPushButton(self)
        self.clear_search.move(425, 450)
        self.clear_search.setText('Сбросить')
        self.clear_search.clicked.connect(self.clear_search_func)
        self.clear_search.setFocusPolicy(Qt.ClickFocus)
        self.show_index = QCheckBox(self)
        self.show_index.setText('Показывать индекс')
        self.show_index.move(580, 475)
        self.show_index.setFocusPolicy(Qt.ClickFocus)
        self.show_index.stateChanged.connect(self.update_index)

    def map_click(self, event):
        pos = (event.pos().x(), event.pos().y())
        try:
            x = float(self.x_field.text())
            y = float(self.y_field.text())
            sz = self.size
        except Exception:
            return
        self.points = [str(x + ((pos[0] / 600) * 2 - 1) * sz), str(y - ((pos[1] / 450) * 2 - 1) * sz * 0.5)]
        res = search_query_coord(','.join(self.points))
        if not res:
            self.addres_output.setText('Не найдено')
            return
        self.addres_output.setText(
            res['address'] + ('' if not self.show_index.checkState() else ' Почтовый индекс: ' + res['index']))
        self.update_image()

    def closeEvent(self, event):
        try:
            os.remove(self.map_file)
        except Exception:
            return

    def update_index(self, event):
        if not len(self.points):
            return
        res = search_query_coord(','.join(self.points))
        if not res:
            self.addres_output.setText('Не найдено')
            return
        self.addres_output.setText(
            res['address'] + ('' if not self.show_index.checkState() else ' Почтовый индекс: ' + res['index']))
        self.update_image()

    def clear_search_func(self):
        self.points = []
        self.addres_output.setText('')
        self.update_image()

    def update_image(self):
        x = self.x_field.text()
        y = self.y_field.text()
        self.getImage(x + ',' + y)
        self.image.setPixmap(QPixmap(self.map_file))
        self.x_field.clearFocus()
        self.y_field.clearFocus()
        self.start.clearFocus()
        self.map_type.clearFocus()
        self.search_input.clearFocus()
        self.search.clearFocus()
        self.clear_search.clearFocus()
        self.addres_output.clearFocus()
        self.show_index.clearFocus()
        self.clearFocus()

    def search_func(self):
        query = self.search_input.text()
        res = search_query_coord(query)
        if not res:
            self.addres_output.setText('Не найдено')
            return
        coords = res['coords']
        self.addres_output.setText(res['address'] + ('' if not self.show_index.checkState() else ' Почтовый индекс: ' + res['index']))
        self.x_field.setText(coords[0])
        self.y_field.setText(coords[1])
        self.points = [coords[0], coords[1]]
        self.update_image()

    def search_point(self):
        x = self.x_field.text()
        y = self.y_field.text()
        self.points = [x, y]
        self.update_image()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_PageDown:
            self.size = min(50, self.size * 2)
            self.update_image()
        elif event.key() == Qt.Key_PageUp:
            self.size = max(0.0001, self.size / 2)
            self.update_image()
        elif event.key() == Qt.Key_Left:
            self.x_field.setText(
                str(min(max(-180 + self.size, float(self.x_field.text()) - self.size * 2), 180 - self.size)))
            self.update_image()
        elif event.key() == Qt.Key_Right:
            self.x_field.setText(
                str(min(max(-180 + self.size, float(self.x_field.text()) + self.size * 2), 180 - self.size)))
            self.update_image()
        elif event.key() == Qt.Key_Up:
            self.y_field.setText(
                str(min(max(-90 + self.size, float(self.y_field.text()) + self.size), 90 - self.size)))
            self.update_image()
        elif event.key() == Qt.Key_Down:
            self.y_field.setText(
                str(min(max(-90 + self.size, float(self.y_field.text()) - self.size), 90 - self.size)))
            self.update_image()


if __name__ == '__main__':
    sys.excepthook = lambda cls, exception, traceback: sys.__excepthook__(cls, exception, traceback)
    app = QApplication(sys.argv)
    ex = Example()
    ex.show()
    sys.exit(app.exec())
