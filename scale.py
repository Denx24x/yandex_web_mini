def get_scale(toponym):
    bf = toponym['boundedBy']['Envelope']
    toponym_coodrinates = toponym["Point"]["pos"]
    lcorner, ucorner = list(map(float, bf['lowerCorner'].split())), list(map(float, bf['upperCorner'].split()))
    toponym_longitude, toponym_lattitude = toponym_coodrinates.split(" ")
    x, y = float(toponym_longitude), float(toponym_lattitude)
    deltax = max(abs(x - lcorner[0]), abs(x - ucorner[0])) * 2
    deltay = max(abs(y - lcorner[1]), abs(y - ucorner[1])) * 2
    return str(deltax), str(deltay)
