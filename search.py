import os
import sys

import pygame
import requests


def search_query_coord(query):
    response = requests.get(
        "http://geocode-maps.yandex.ru/1.x/?apikey=40d1649f-0493-4b70-98ba-98533de7710b&geocode=" + query + "&format=json")
    if not response:
        return False
    json_response = response.json()
    try:
        toponym = json_response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]
    except Exception:
        return False
    toponym_address = toponym["metaDataProperty"]["GeocoderMetaData"]["text"]
    try:
        toponym_index = toponym["metaDataProperty"]['GeocoderMetaData']['Address']['postal_code']
    except:
        toponym_index = 'Почтовый индекс отсутствует'
    toponym_coodrinates = toponym["Point"]["pos"]
    return {'coords': toponym_coodrinates.split(), 'address': toponym_address, 'index': toponym_index}
